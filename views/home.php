
    <div class="main-content" >
        <div class="head left container" >
            <div class="row" >
                <div class="col-md-8" >
                    <h2 class="title" >Benvenuto Dr. carmine.rumma@healthtouch.eu</h2>
                    <h4 class="subtitle">Nam libero tempore, cum soluta nobis est eligendi optio maxime possimus,
                        omnis voluptas assumenda est, omnis dolor repellendus.<br>{{ $user->access_token }}</h4>
                </div>
                <div class="col-md-4 text-right" >

                    <a href="#?" class="button secondary new-tv button--new-vs">
                        <?php include DOCROOT . "/assets/dist/img/clock-1.svg"; ?>
                        Nuovo videoconsulto
                    </a>

                </div>
            </div>
        </div>

        <?php include_once VIEWS_INCLUDES_PATH . "/video_session.php"; ?>

        <div class="tele-list" id="televApp" >

            <h2>Teleconsulti in programma</h2>

            <div class="row" >
                <div class="container" >
                    <table>
                        <thead>
                            <tr>
                                <?php /*
                               <th width="22%">Nome Paziente</th>
                               <th width="23%">N° polizza</th>
                               <th width="15%">Data e ora</th>
                              <!-- <th width="25%">Note</th>-->
                                <th width="15%">&nbsp;</th>
                                */?>
                                <th width="22%">Nome Paziente</th>
                                <th width="23%">N° polizza</th>
                                <th width="45%">Data e ora</th>
                                <!-- <th width="25%">Note</th>-->
                                <th width="15%">&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>

    </div>


<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DMR DASHBOARD</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com" >
    <link href="//unpkg.com/ionicons@4.1.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <?php require_once VIEWS_INCLUDES_PATH . "/header.php"; ?>

</head>
<body>
    <div id="app" >
        <nav class="ddh-header">
            <div class="container" >
                <a class="ddh-logo" href="/">
                   <img src="/assets/dist/img/logo.svg" />
                </a>


                <ul class="nav navbar-nav navbar-right">
                        <?php if (Helper::isLogged()) { ?>
                    <?php /*
                        <li>
                            <a class="nav-link" href="{{ route('login') }}">
                                @svg('button-power-1-big', 'icon') {{ __('Login') }}
                            </a>
                        </li>

                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>--}}

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $user->email }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                          */ ?>

                            <li>
                                <a class="nav-link light button--new-vs" href="#?" >
                                    <?php include DOCROOT . "/assets/dist/img/clock-1.svg"; ?>
                                    Nuovo Videoconsulto
                                </a>
                            </li>
                            <li>
                                <a class="nav-link light" href="/logout"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <?php include DOCROOT . "/assets/dist/img/big-icon-power.svg"; ?>
                                    Logout
                                </a>
                                <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>

                            <?php } else { ?>

                            <?php } ?>
                    </ul>

                </div>
            </nav>

            <main class="ddash-main" >
                <?php include $content; ?>
            </main>
        </div>

        <script type="text/javascript" >

        </script>

    </body>
</html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="Viewport" name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

<script src="assets/dist/js/plugins/jquery-2.2.1.min.js" ></script>

<!-- DatePicker -->
<script src="assets/dist/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="assets/dist/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" >
<!-- DatePicker -->

<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" >
<!-- DataTables -->

<!-- Autocomplete -->
<script src="assets/dist/js/plugins/jquery.autocomplete.min.js"></script>
<!-- Autocomplete -->

<!-- Mask -->
<script src="assets/dist/js/plugins/jquery.mask.min.js"></script>
<!-- Mask -->

<!-- Validate -->
<script src="assets/dist/js/plugins/jquery.validate/jquery.validate.min.js"></script>
<script src="assets/dist/js/plugins/jquery.validate/messages_it.js"></script>
<!-- Validate -->

<script type="text/javascript" >
   // window.vsUrl = "{{ route('vs-more') }}";
    window.vsUrl = "ajax_sample.php";
    window.vsQCreate = "{{ route('vs-qcreate') }}";
    window.vsCreate = "{{ route('vs-create') }}";
    $(function(){
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            var ww = ( $(window).width() < window.screen.width ) ? $(window).width() : window.screen.width;
            var mw = 480;
            var ratio =  ww / mw;
            if( ww < mw){
                $('#Viewport').attr('content', 'initial-scale=' + ratio + ', maximum-scale=' + ratio + ', minimum-scale=' + ratio + ', user-scalable=yes, width=' + ww);
            } else {
                $('#Viewport').attr('content', 'initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=' + ww);
            }
        }
    });
</script>

<script src="https://requirejs.org/docs/release/2.3.5/minified/require.js" ></script>


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="assets/dist/css/app.css?<?php echo time(); ?>" rel="stylesheet" >


<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

 <!--[if lt IE 9]>
   <script src = "http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->


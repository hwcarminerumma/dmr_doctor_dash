<div class="row hidden" id="new-video-session">
    <div class="container" >

        <!-- cluster -->
        <div class="cluster-panel" >
            <h3 class="cluster-title">Nuovo teleconsulto</h3>

            <div class="cluster" >
                <div class="wrapper" >
                    <form action="#?" name="vs" id="vs" >
                        <!-- quick -->
                        <div class="quick" >
                            <div class="col-md-6" >
                                <div class="form-group" >
                                    <label for="password" class="abs" >N° polizza paziente</label>
                                    <input type="text" class="form-control" name="bill_number_quick" id="qautocomplete" />
                                </div>
                            </div>
                            <div class="col-md-6 actions" >
                                <div class="form-group" >
                                    <button type="submit" class="button secondary set_time">
                                        Imposta data e ora
                                    </button>
                                    <button type="submit" class="button cta" id="quicksave_vs_btn" >
                                        Avvia ora
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- /quick -->

                        <!-- fillable -->
                        <div class="fillable" >
                            <div class="col-md-6" >
                                <div class="form-group" >
                                    <label for="password" class="abs" >N° polizza paziente</label>
                                    <input type="text" class="form-control" name="bill_number" id="autocomplete" />
                                </div>
                            </div>
                            <div class="col-md-6 actions" >
                                <div class="row" >
                                    <div class="col-xs-7" >
                                        <div class="form-group has-icon" >
                                            <label for="data" class="abs" >Data</label>
                                            <input type="text" class="form-control" name="planned_date" data-provide="datepicker" />
                                            <i class="" >
                                                <img src="assets/dist/img/calendar-1.png" />
                                            </i>
                                        </div>
                                    </div>

                                    <div class="col-xs-5" >
                                        <div class="form-group has-icon" >
                                            <label for="data" class="abs" >Ora</label>
                                            <input type="text" class="timepicker form-control" name="planned_time" />
                                            <i class="" >
                                                <img src="assets/dist/img/clock-1.png" />
                                            </i>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .row/ -->
                            <div class="separator" ></div>
                            <div class="col-md-12" >
                                <div class="form-group" >
                                    <label for="note" class="abs" >Note</label>
                                    <textarea class="form-control" name="notes"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /fillable -->

                    </form>
                </div>

            </div>
            <div class="insert--buttons" >
                <button type="submit" class="button cta" id="save_vs_btn">
                    Salva
                </button>
                <button type="reset" class="button reset">
                    Annulla
                </button>
            </div>
        </div>
        <!-- cluster -->

    </div>
</div>

<script type="text/javascript" >
    (function ($){
        $('.timepicker').mask('00:00');
        // Autocomplete
        $('#qautocomplete, #autocomplete').devbridgeAutocomplete({
            serviceUrl: '/get-userlist',
            onSearchStart: function (params){
                var $loader = $(this).parent().find(".aloader");
                if ($loader.length == 0)
                    $("<i class='fa fa-circle-o-notch aloader'></i>").insertAfter($(this));

                $loader.removeClass("_hide");
            },
            onSearchComplete: function (){
                $(this).next().addClass("_hide");
            },
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });
    })(jQuery);
</script>

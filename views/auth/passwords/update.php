@extends('layouts.app')

@section('content')
    <div class="main-content" >
        <div class="head" >
            <h2 class="title" >{{ __('Update your password') }}</h2>
        </div>

        <div class="row" >
            <div class="container" >

                <div class="col-xs-12 col-md-offset-3 col-md-6 widget-box login">
                    <div class="widget-content" >
                        <div class="widget-padding">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.update') }}" id="frm">
                                @csrf

                                <div class="form-group">
                                    <label for="email" class="col-md-12" for="email">{{ __('Inserisci qui la nuova password') }}</label>

                                        <input id="pwd" type="text" class="form-control{{ $errors->has('pwd') ? ' is-invalid' : '' }}" name="pwd" value="" required>

                                        @if ($errors->has('pwd'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('pwd') }}</strong>
                                            </span>
                                        @endif

                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-12" for="email">{{ __('Conferma la nuova password') }}</label>
                                    <input id="pwd_confirm" type="text" class="form-control{{ $errors->has('pwd_confirm') ? ' is-invalid' : '' }}" name="pwd_confirm" value="" required>

                                    @if ($errors->has('pwd_confirm'))
                                        <span class="invalid-feedback">
                                                <strong>{{ $errors->first('pwd_confirm') }}</strong>
                                        </span>
                                    @endif

                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-xs-12 text-center">
                                        <button id="submit" type="submit" class="button">
                                            {{ __('Salva Password') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" >

        $( "#frm" ).validate({
            rules: {
                pwd: "required",
                pwd_confirm: {
                    equalTo: "#pwd"
                }
            }
        });

    </script>

@endsection

@extends('layouts.app')

@section('content')
    <div class="main-content" >
        <div class="head" >
            <h2 class="title" >{{ __('Reset password') }}</h2>
            <h4 class="subtitle">Ti abbiamo inviato una mail con il codice di sicurezza.<br>Inserisci il codice di seguito</h4>
        </div>

        <div class="row" >
            <div class="container" >

                <div class="col-xs-12 col-md-offset-3 col-md-6 widget-box login">
                    <div class="widget-content" >
                        <div class="widget-padding">
                            @if (session('status'))
                                <div class="alert alert-danger">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.verify') }}">
                                @csrf

                                <div class="form-group">
                                    <label for="email" class="col-md-12" for="email">{{ __('Inserisci il codice ricevuto') }}</label>

                                    <div class="code-group" >
                                        @for ($i = 0; $i < 6; $i++)
                                         <input type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="char{{$i}}" value="" required maxlength="1">
                                        @endfor
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="button check">
                                            {{ __('Verifica') }}
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" >

        console.log({!! $code !!});

    </script>

@endsection

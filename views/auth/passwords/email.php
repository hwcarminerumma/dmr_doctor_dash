
    <div class="main-content" >
        <div class="head" >
            <h2 class="title" >Reset password</h2>
            <h4 class="subtitle">Nam libero tempore, cum soluta nobis est eligendi optio maxime possimus,
                omnis voluptas assumenda est, omnis dolor repellendus.</h4>
        </div>

        <div class="row" >
            <div class="container" >

                <div class="col-xs-12 col-md-offset-3 col-md-6 widget-box login">
                    <div class="widget-content" >
                        <div class="widget-padding">
                            <?php if(!empty($errors_email)){ ?>
                                <div class="alert alert-danger">
                                    Errore..
                                </div>
                            <?php } ?>
                            <form method="POST" action="">


                                <div class="form-group">
                                    <label for="email" class="col-md-12" for="email">Email (username)</label>

                                        <input id="email" type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="" required>

                                        <?php if(!empty($errors_email)){ ?>
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email</strong>
                                            </span>
                                        <?php } ?>

                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="button">
                                            Send Password Reset Link
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

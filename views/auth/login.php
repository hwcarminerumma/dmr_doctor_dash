<div class="main-content" >
    <div class="head" >
        <h2 class="title" >Login</h2>
        <h4 class="subtitle">Nam libero tempore, cum soluta nobis est eligendi optio maxime possimus,
    omnis voluptas assumenda est, omnis dolor repellendus.</h4>
    </div>

	<div class="row" >
        	<div class="container" >

        		<div class="col-xs-12 col-md-offset-3 col-md-6 widget-box login">
        			<div class="widget-content" >
        				<div class="widget-padding">
							<?php if(!empty($errors)) { ?>
                                <div class="col-md-12" >
                                    <div class="alert alert-danger">
                                    {{ implode('', $errors->all(':message')) }}
                                    </div>
								</div>
							<?php } ?>
        					<form method="POST" action="/login" >

        						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        							<div class="col-md-12" >
                                        <label for="email" >Email (username)</label>
        								<input id="email" type="text" class="form-control" name="email" value="" placeholder="" required autofocus>

                                        <?php if(!empty($error_email)) { ?>
        									<span class="help-block">{{ $errors->first('email') }}</span>
                                        <?php } ?>
        							</div>
        						</div>

        						<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        							<div class="col-md-12">
                                        <label for="password" >Password</label>
        								<input id="password" type="password" class="form-control" name="password" placeholder="" required>

                                        <?php if(!empty($error_password)) { ?>
        									<span class="help-block">{{ $errors->first('password') }}</span>
                                        <?php } ?>
        							</div>
        						</div>

        						<div class="form-group actions" >
        							<div class="row">
        								<div class="col-xs-12 text-right">
        									<a class="btn btn-link" href="/password-reset">
                                                Dimenticato la password?
        									</a>
                                        </div>
                                        <?php /*
        								<div class="col-xs-12">
        									<div class="checkbox">
        										<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><span>Maintain connection</span>
        									</div>
        								</div>
        								*/ ?>
        							</div>
        							<div class="row signup">

                                        <div class="col-xs-12">
        									<button type="submit" class="button">
                                                Accedi
        									</button>
        								</div>
										<?php /*
                                        <div class="col-xs-12">
                                            <p>Non hai un account?</p>
                                            <a href="#?" class="btn-link" >Registrati</a>
                                        </div>
                                        */ ?>
        							</div>
        						</div>
        					</form>

        				</div>
        			</div>
        		</div>
        	</div>
	</div>
</div>

//var $ = require( 'jquery' );
//require( 'datatables.net' )( window, $ );
        //window.Vue = require('vue');


        //require('bootstrap-sass');
        var moment = require('moment');
        require('jquery-countdown');
        //var dt = require('datatables.net')();

//import 'datatables.net-dt/css/jquery.datatables.css';


        (function ($) {
            $(".set_time").bind("click", function (e) {
                e.preventDefault();

                var $quickBillNumber = $("input[name='bill_number_quick']");
                var $BillNumber = $("input[name='bill_number']");
                if ($quickBillNumber.val() != "") {
                    $BillNumber.val($quickBillNumber.val());
                    setTimeout(function () {
                        //$BillNumber.focus();
                    }, 400);
                }
                var $cluster = $(this).parent().parent().parent().parent().parent().parent();
                var $clusterPanel = $cluster.parent();
                $cluster.addClass("fill");
                $clusterPanel.addClass("spacing");
            });
            $(".reset").bind("click", function (e) {
                e.preventDefault();
                var $cluster = $(this).parent().prev();
                var $clusterPanel = $cluster.parent();
                $cluster.removeClass("fill");
                $clusterPanel.removeClass("spacing");
            });

            $(".button--new-vs").bind("click", function (e) {
                e.preventDefault();
                $("html,body").animate({scrollTop: $("#new-video-session").position().top + 100})
                $("#new-video-session").removeClass("hidden");
            });

            // Televisit Video Sessions
            var vsTable = $('#televApp table').DataTable({
                "processing": true,
                "serverSide": true,
                autoWidth: false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "pageLength": 50,
                "bAutoWidth": false,
                "paging": true,
                "ordering": false,
                "info": false,
                "searching": false,
                "ajax": window.vsUrl,
                "columns": [
                    {"data": "patient_name"},
                    {"data": "bill_number"},
                    {"data": "planned_at"},
                    //{ "data": "notes" },
                    {"data": "id", "class": "center"}
                ],
                "language": {
                    "lengthMenu": "Display _MENU_ records per page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing page _PAGE_ of _PAGES_",
                    "infoEmpty": "Nessuna televisita in programma",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                columnDefs: [
                    /*
                    {
                        "render": function ( data, type, row ) {
                            return '<div class="notes" >' + data + "</div>";
                        },
                        "targets": 3
                    },
                    */
                    {
                        "render": function (data, type, row) {
                            var untilTxt = "";

                            let now = moment();
                            var end = moment(row.planned_at);
                            var startBtnStatus = "disabled";
                            startBtnStatus = ""; // test
                            if (end.date() == now.date() && end.month() == now.month()) {
                                //console.log(moment(now,"DD/MM/YYYY HH:mm:ss"), moment(end,"DD/MM/YYYY HH:mm:ss"));
                                //var dv = moment.utc(moment(now,"DD/MM/YYYY HH:mm:ss").diff(moment(end,"DD/MM/YYYY HH:mm:ss")));

                                startBtnStatus = "";
                                var momEnd = moment(end, "DD/MM/YYYY HH:mm:ss");
                                var momNow = moment(now, "DD/MM/YYYY HH:mm:ss");
                                var dv = moment.utc(momEnd.diff(momNow));
                                var untilClass = [];

                                if (momNow > momEnd) {
                                    untilClass.push("expired");
                                } else {
                                    untilClass.push(dv.hours() <= 1 ? 'countdown' : '');
                                }
                                untilTxt = '<span data-d="' + moment(end, "DD/MM/YYYY HH:mm:ss") + '" class="' + untilClass.join(' ') + '" >' + dv.format("HH:mm:ss") + '</span>';
                            }

                            var startBtn = '<a href="#?" ' + startBtnStatus + ' data-id="' + row.room + '" class="button startTV" >AVVIA</a>';

                            return startBtn + untilTxt;
                        },
                        "targets": 3
                    }
                ],
                "initComplete": function (settings, json) {

                    $(".countdown").each(function () {
                        var d = $(this).attr("data-d");
                        $(this).countdown(d, function (event) {
                            $(this).html(event.strftime('%H:%M:%S'));
                        });
                    });


                }
            });

            $(document).on("click", ".startTV", function (e) {
                e.preventDefault();
                tv.open($(this).attr("data-id"));
            })

            $(document).on("click", "#save_vs_btn", function (e) {
                e.preventDefault();
                var $this = $(this);
                $.ajax({
                    url: window.vsCreate,
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: $("#vs").serialize(),
                    success: function (response) {
                        var res = $.parseJSON(response);
                        if (res.status == "success") {
                            $('#vs')[0].reset();
                            vsTable.ajax.reload();
                            var $cluster = $this.parent().prev();
                            var $clusterPanel = $cluster.parent();
                            $cluster.removeClass("fill");
                            $clusterPanel.removeClass("spacing");
                        }
                    }
                });
            });

            var tv = {
                open: function (room_id) {
                    //alert("Opening televisit with handshake.." + handshake);
                    window.location.href = "/televisit?room_id=" + room_id
                }
            }

            $(document).on("click", "#quicksave_vs_btn", function (e) {
                e.preventDefault();
                var $this = $(this);
                tv.open(123456);
                return;
                $.ajax({
                    url: window.vsQCreate,
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: $("#vs").serialize(),
                    success: function (response) {
                        var res = $.parseJSON(response);
                        if (res.status == "success") {
                            $('#vs')[0].reset();
                            vsTable.ajax.reload();
                            var $cluster = $this.parent().prev();
                            var $clusterPanel = $cluster.parent();
                            $cluster.removeClass("fill");
                            $clusterPanel.removeClass("spacing");
                            tv.open(res.room_id);
                        }
                    }
                });
            });

            $('.code-group input').bind('input propertychange', function () {
                var jump = false;
                if ($.isNumeric($(this).val())) {
                    jump = true;
                }
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                if (jump == true) {
                    if ($(this).next().prop("tagName") == "INPUT")
                        $(this).next().focus();
                }
            });


        })(jQuery);


<?php
/**
 * Created by PhpStorm.
 * User: crumma
 * Date: 2019-07-26
 * Time: 12:46
 */

define ("DOCROOT", $_SERVER["DOCUMENT_ROOT"]);
define ("CONTROLLERS_PATH", DOCROOT . "/controllers");
define ("VIEWS_PATH", DOCROOT . "/views");
define ("VIEWS_INCLUDES_PATH", DOCROOT . "/views/includes");
require_once "./Helper.php";
Helper::$user_logged = false;

$params = array_keys($_GET);
if (!empty($_GET['ctrl'])){

    $action = "index";
    if (!empty($_GET['a']))
        $action = $_GET['a'];

    $className = ucfirst($_GET['ctrl']) . "Controller";
    require_once CONTROLLERS_PATH . "/" . $className . ".php";
    $controller = new $className();

    $content = $controller->$action();

} else {
    $content = VIEWS_PATH . "/auth/login.php";
}

include_once (dirname(__FILE__) . "/views/layouts/app.php");
?>


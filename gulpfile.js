'use strict';
/**
 * @author Carmine Rumma
 *
 */

const fs   				= require('fs');
const gutil             = require('gulp-util');
const gulp              = require('gulp');
const sass              = require('gulp-sass');
const browserSync       = require('browser-sync').create();
const rjs               = require('gulp-requirejs');
const minify            = require('gulp-minify');
const prefix            = require('gulp-autoprefixer');
const minifyCSS         = require('gulp-minify-css');
const imagemin          = require('gulp-imagemin');
const browserify        = require('gulp-browserify');
const uglify            = require('gulp-uglify');

const gulpSequence      = require('gulp-sequence').use(gulp);
const shell             = require('gulp-shell');
const plumber           = require('gulp-plumber');
const concat            = require('gulp-concat');
const autoprefixer      = require('gulp-autoprefixer');
const sourceMaps        = require('gulp-sourcemaps');
const autoPrefixBrowserList = ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'];
const rollup 				= require('gulp-better-rollup');
const babel 				= require('gulp-babel');
const resolve 				= require('rollup-plugin-node-resolve');
const commonjs 				= require('rollup-plugin-commonjs');
const webpack 				= require('webpack-stream');
var requirejsOptimize 		= require('gulp-requirejs-optimize');
var babelify 				= require("babelify");
var notify 					= require('gulp-notify'),
	watch 					= require('gulp-watch'),
	sourcemaps 				= require('gulp-sourcemaps');

var src_path 		= "./assets/src/";
var deploy_path 	= "./assets/dist/";

var styles_src = src_path + "scss";
var scripts_src = src_path + "js";

var images_src = src_path + "img";
var images_dest = deploy_path + "img";

var styles_dest = deploy_path + "css";
var scripts_dest = deploy_path + "js";

var fonts_src = src_path + "webfonts";
var fonts_dest = deploy_path + "webfonts";


/*
gulp.task('compress', function() {
    gulp.src(['./assets/js/*.js'])
        .pipe(minify())
        .pipe(gulp.dest('dist/js'))
});
*/
gulp.task('scripts', function() {

	function handle_error(err) {
		notify.onError({
			"title": "Scripts error.",
			"message": "<%= error.message %>"
		})(err);
		console.log(err);
		this.emit('end');
	}
	gulp.src([scripts_src + '/plugins/**']).pipe(gulp.dest(scripts_dest + '/plugins/'));
	gulp.src([scripts_src + '/app.js'])

		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: handle_error
		}))
		.pipe(babel({
			presets: ['@babel/preset-env']
		}))
		.pipe(browserify({
			insertGlobals : true,
			debug : !gulp.env.production
		}))
		//.pipe(minify())
		//.pipe(uglify())
		.pipe(concat("app.js"))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(scripts_dest + "/"))
		.pipe(notify({
			"title": "Scripts compiled.",
			"message": "Compiled <%= file.relative %>"
		}))
});

gulp.task('scripts-deploy', function() {

	function handle_error(err) {
		notify.onError({
			"title": "Scripts error.",
			"message": "<%= error.message %>"
		})(err);
		console.log(err);
		this.emit('end');
	}
	gulp.src([scripts_src + '/plugins/**']).pipe(gulp.dest(scripts_dest + '/plugins/'));
	gulp.src([scripts_src + '/app.js'])

		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: handle_error
		}))
		.pipe(babel({
			presets: ['@babel/preset-env']
		}))
		.pipe(browserify({
			insertGlobals : true,
			debug : !gulp.env.production
		}))
		//.pipe(minify())
		.pipe(uglify())
		.pipe(concat("app.js"))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(scripts_dest + "/"))
		.pipe(notify({
			"title": "Scripts compiled.",
			"message": "Compiled <%= file.relative %>"
		}))
});

gulp.task('bs-reload', () => {
	return browserSync.reload();
});


gulp.task("build", function(){
	return browserify({
		entries: [scripts_src + '/app.js']
	})
	.transform(babelify)
	.bundle()
	.pipe(source("bundle.js"))
	.pipe(gulp.dest(scripts_dest + "/"));
});

//compressing images & handle SVG files
gulp.task('images', function(tmp) {
	gulp.src([images_src + '/**'])
		.pipe(plumber())
		.pipe(imagemin({
			progressive: true
		}))
		.on('error', function (e){
			console.log(e);
		})
		.pipe(gulp.dest(images_dest + "/"));
});

//compressing images & handle SVG files
gulp.task('images-deploy', function() {
	gulp.src([images_src + '/**'])
		.pipe(plumber())
		.pipe(gulp.dest(images_dest));
});

gulp.task('browser-sync', () => {
	/*
	return browserSync.init({
		server: {
			baseDir: './'
		},
		ghostMode: true
	});
	*/
});

gulp.task('browsersync-reload', function () {
	browserSync.reload();
});


/*
gulp.task('sass', function () {
    gulp.src(styles_src + '/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(gulp.dest(styles_dest));
});
*/

gulp.task('styles', function() {

	return gulp.src(styles_src + '/*.scss')
	//prevent pipe breaking caused by errors from gulp plugins
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		//get sourceMaps ready
		//.pipe(sourceMaps.init())
		//include SCSS and list every "include" folder
		.pipe(sass({
			errLogToConsole: true,
			includePaths: [
				styles_src
			]
		}))
		.pipe(autoprefixer({
			//browsers: autoPrefixBrowserList,
			cascade:  true
		}))
		//catch errors
		.on('error', gutil.log)
		//the final filename of our combined css file
		// .pipe(concat('styles.css'))
		//get our sources via sourceMaps
		.pipe(sourceMaps.write())
		//where to save our final, compressed css file
		.pipe(gulp.dest(styles_dest))
		//notify browserSync to refresh
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('fonts', function() {
	//the initializer / master SCSS file, which will just be a file that imports everything
	return gulp.src(fonts_src + '/**')
	//prevent pipe breaking caused by errors from gulp plugins
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))

		//catch errors
		.on('error', gutil.log)
		.pipe(gulp.dest(fonts_dest))
		//notify browserSync to refresh
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('styles-deploy', function() {
	//the initializer / master SCSS file, which will just be a file that imports everything
	return gulp.src(styles_src + '/*.scss')
		.pipe(plumber())
		//include SCSS includes folder
		.pipe(sass({
			includePaths: [
				styles_src
			]
		}))
		.pipe(autoprefixer({
			browsers: autoPrefixBrowserList,
			cascade:  true
		}))
		//the final filename of our combined css file
		//.pipe(concat('styles.css'))
		.pipe(minifyCSS())
		//where to save our final, compressed css file
		.pipe(gulp.dest(styles_dest));
});



gulp.task('watch', ['scripts', 'styles', 'fonts', 'images'], function(){
	//gulp.watch(styles_src + '*.scss', ['sass']).on('change', browserSync.reload);
	//gulp.watch('./assets/js/*.js', ['compress']).on('change', browserSync.reload);
	gulp.watch(styles_src + '/**', ['styles']);
	gulp.watch(scripts_src + '/**', ['scripts']);
	gulp.watch(images_src + '/**', ['images']);
	gulp.watch(fonts_src + '/**', ['fonts']);
});

gulp.task('clean', function() {
	return shell.task([
		'rm -rf ' + deploy_path
	]);
});

gulp.task('scaffold', function() {
	var folders = [
		deploy_path,
		deploy_path + "fonts",
		deploy_path + "img",
		deploy_path + "js",
		deploy_path + "js/plugins",
		deploy_path + "css",
		deploy_path + "webfonts"
	];
	folders.forEach(dir => {
		if(!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
			console.log('📁  folder created:', dir);
		}
	});
/*
	return shell.task([
			'mkdir ' + deploy_path,
			'mkdir ' + deploy_path + "fonts",
			'mkdir ' + deploy_path + "img",
			'mkdir ' + deploy_path + "js",
			'mkdir ' + deploy_path + "css"
		]
	);
	*/
});

//gulp.task('default', ['watch']);
gulp.task('watch', ['scripts', 'browser-sync'], () => {
	gulp.watch(styles_src + "/*.scss", ['styles']);
	gulp.watch(scripts_src + "/*.js", ['scripts']);
	gulp.watch('*.html', ['bs-reload']);
});

gulp.task('deploy', gulpSequence('clean', 'scaffold', ['images', 'fonts', 'scripts-deploy', 'styles-deploy', 'images-deploy']));
